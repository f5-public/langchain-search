import streamlit as st
#from langchain.llms.openai import OpenAI
from langchain_openai import OpenAI
from langchain.agents import load_tools, initialize_agent
#from langchain.utilities import SerpAPIWrapper
from langchain_community.utilities import SerpAPIWrapper
from langchain import hub
from langchain.agents import AgentExecutor, create_self_ask_with_search_agent
import os
from langchain.agents import Tool
from langchain.agents import AgentType
#from langchain.utilities import SearchApiAPIWrapper



# Streamlit app
st.subheader('LangChain Search')

# Get OpenAI API key, SERP API key and search query
with st.sidebar:
    serpapi_api_key = st.text_input("SerpAPI API Key", type="password")
    st.caption("*If you don't have a SerpAPI API key, get it [here](https://serpapi.com/).*")
search_query = st.text_input("Search Query")

openai_api_base = os.environ.get("OPENAI_API_BASE", "http://llama.llm.svc.cluster.local:8000/v1")

# If the 'Search' button is clicked
if st.button("Search"):
    # Validate inputs
    if not serpapi_api_key.strip() or not search_query.strip():
        st.error(f"Please provide the missing fields.")
    else:
        try:
            os.environ["SERPAPI_API_KEY"] = serpapi_api_key
            with st.spinner('Please wait...'):
              # Initialize the OpenAI module, load the SerpApi tool, and run the search query using an agent
              llm=OpenAI(temperature=0, openai_api_key="bla", openai_api_base=openai_api_base, verbose=True)
              #search = SearchApiAPIWrapper()
              params = {
                "engine": "google",
                "gl": "us",
                "hl": "en",
              }
              search = SerpAPIWrapper(params=params)
              tools = [
                Tool(
                    name="Intermediate Answer",
                    func=search.run,
                    description="useful for when you need to ask with search",
                )
              ]
              # agent = initialize_agent(tools, llm, agent=AgentType.SELF_ASK_WITH_SEARCH, verbose=True)
              # result = agent.run(search_query)
              prompt = hub.pull("hwchase17/self-ask-with-search")
              agent = create_self_ask_with_search_agent(llm, tools, prompt)
              agent_executor = AgentExecutor(agent=agent, tools=tools, verbose=True, handle_parsing_errors=True)
              result = agent_executor.invoke({"input": search_query})
              st.success(result)
        except Exception as e:
            st.exception(f"An error occurred: {e}")
